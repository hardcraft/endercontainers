package fr.utarwyn.endercontainers.utils;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;

public class ItemSerializer {

    public static HashMap<Integer, ItemStack> stringToItems(String data){

        try {
            HashMap<Integer, ItemStack> items = new HashMap<>();
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            int size = dataInput.readInt();

            // Read the serialized inventory
            for (int i = 0; i < size; i++) {
                items.put(i, (ItemStack) dataInput.readObject());
            }

            dataInput.close();
            return items;
        } catch (ClassNotFoundException | IOException e) {
            throw new IllegalStateException("Unable to load item stacks.", e);
        }

    }


    public static String itemsToString (HashMap<Integer, ItemStack> items){

        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);


            int size = items.size();

            // Write the size of the inventory
            dataOutput.writeInt(size);

            // Save every element in the list
            for (int i = 0; i < size; i++) {
                dataOutput.writeObject(items.get(i));
            }

            // Serialize that array
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }

    }

}
