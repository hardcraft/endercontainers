package fr.utarwyn.endercontainers.listeners;

import fr.utarwyn.endercontainers.EnderChest;
import fr.utarwyn.endercontainers.EnderContainers;
import fr.utarwyn.endercontainers.containers.MenuContainer;
import fr.utarwyn.endercontainers.managers.EnderchestsManager;
import fr.utarwyn.endercontainers.utils.Config;
import fr.utarwyn.endercontainers.utils.CoreUtils;
import fr.utarwyn.endercontainers.utils.EnderChestUtils;
import fr.utarwyn.endercontainers.utils.PluginMsg;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class EnderChestListener implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        Block b = e.getClickedBlock();

        if (!e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
        if (b == null) return;

        if (b.getType().equals(Material.ENDER_CHEST)) {

            e.setCancelled(true);

            if (Config.enabled){
                playSoundTo(Config.openingChestSound, p);
                EnderContainers.getEnderchestsManager().openPlayerMainMenu(p, null);
            }else{
                PluginMsg.pluginDisabled(p);
            }
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        String invname = e.getInventory().getTitle();

        if (!(e.getInventory().getHolder() instanceof MenuContainer)) return;

        Player playerOwner = EnderContainers.getEnderchestsManager().getLastEnderchestOpened(p);
        Sound clickSound   = CoreUtils.soundExists("CLICK") ? Sound.valueOf("CLICK") : Sound.valueOf("UI_BUTTON_CLICK");

        if (invname.equalsIgnoreCase(CoreUtils.replacePlayerName(EnderContainers.__("enderchest_main_gui_title"), p))) { // Own main enderchest
            Integer index = e.getRawSlot();

            e.setCancelled(true);

            if(e.getCurrentItem() == null
                || e.getCurrentItem().getType().equals(Material.AIR)
                || index >= e.getInventory().getSize()
                || index < 0
                || index >= Config.maxEnderchests)
                return;


            if (p.hasPermission(Config.enderchestOpenPerm + index) || index < Config.defaultEnderchestsNumber) {
                p.playSound(p.getLocation(), clickSound, 1, 1);
                EnderContainers.getInstance().enderchestsManager.openPlayerEnderChest(index, p, null);
            }else{
                p.playSound(p.getLocation(), Sound.VILLAGER_NO, 1, 2);
            }
        }else if(playerOwner != null && invname.equalsIgnoreCase(CoreUtils.replacePlayerName(EnderContainers.__("enderchest_main_gui_title"), playerOwner))){ // Player who open another enderchest
            Integer index = e.getRawSlot();
            e.setCancelled(true);

            if (index >= e.getInventory().getSize()
                || index < 0
                || index >= Config.maxEnderchests)
                return;

            p.playSound(p.getLocation(), clickSound, 1, 1);

            EnderChest ec = EnderContainers.getEnderchestsManager().getPlayerEnderchest(playerOwner, index);
            if(ec == null || index > (EnderChestUtils.getPlayerAvailableEnderchests(playerOwner) - 1)) return;

            if (p.hasPermission(Config.enderchestOpenPerm + index) || index < Config.defaultEnderchestsNumber)
                EnderContainers.getEnderchestsManager().openPlayerEnderChest(index, p, playerOwner);
        }else if(playerOwner == null){ // Player who open an offline enderchest
            if(EnderContainers.getEnderchestsManager().enderchestsOpens.containsKey(p)) return;

            MenuContainer menu = (MenuContainer) e.getInventory().getHolder();
            Integer index      = e.getRawSlot();
            String playername  = menu.offlineOwnerName;
            UUID uuid          = menu.offlineOwnerUUID;

            if(invname.equalsIgnoreCase(CoreUtils.replacePlayerName(EnderContainers.__("enderchest_main_gui_title"), playername))){

                e.setCancelled(true);

                if(index >= e.getInventory().getSize()
                    || index < 0)
                    return;

                EnderContainers.getEnderchestsManager().openOfflinePlayerEnderChest(index, p, uuid, playername);
            }
        }
    }

    @EventHandler
    public void onInventoryClosed(InventoryCloseEvent e) {
        Player p = (Player) e.getPlayer();
        Inventory inv = e.getInventory();
        EnderChest ec = null;
        EnderchestsManager ecm = EnderContainers.getEnderchestsManager();

        Player playerOwner = ecm.getLastEnderchestOpened(p);

        if(inv.getName().equalsIgnoreCase("container.enderchest")){
            playSoundTo(Config.closingChestSound, p);
            return;
        }

        if (inv.getName().equalsIgnoreCase(CoreUtils.replacePlayerName(EnderContainers.__("enderchest_main_gui_title"), p))) return;
        if (playerOwner != null && inv.getName().equalsIgnoreCase(CoreUtils.replacePlayerName(EnderContainers.__("enderchest_main_gui_title"), playerOwner))) return;
        if (!ecm.enderchestsOpens.containsKey(p)) return;
        ec = ecm.enderchestsOpens.get(p);

        if(ec != null && ec.getOwner() == null){ // Close offline player's chest (or main menu)
            if(inv.getName().equalsIgnoreCase(CoreUtils.replacePlayerName(EnderContainers.__("enderchest_main_gui_title"), ec.ownerName))) return;
        }

        ecm.enderchestsOpens.remove(p);

        assert ec != null;
        ec.clearItems();

        int index = 0;
        for (ItemStack i : inv.getContents()) {
            ec.addItem(index, i);
            index++;
        }

        ec.save();

        EnderContainers.getEnderchestsManager().removeEnderChest(ec);

        playSoundTo(Config.closingChestSound, p);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e){
        EnderContainers.getEnderchestsManager().removeFromLastEnderchestOpened(e.getPlayer());
    }

    private void playSoundTo(String soundName, Player player){
        if(CoreUtils.soundExists(soundName)) {
            player.playSound(player.getLocation(), Sound.valueOf(soundName), 1F, 1F);
        }else
            CoreUtils.log("§cThe sound §6" + soundName + "§c doesn't exists. Please change it in the config.", true);
    }
    private void playSoundInWorld(String soundName, Player player, Location location){
        if(CoreUtils.soundExists(soundName)) {
            player.getWorld().playSound(location, Sound.valueOf(soundName), 1F, 1F);
        }else
            CoreUtils.log("§cThe sound §6" + soundName + "§c doesn't exists. Please change it in the config.", true);
    }
}